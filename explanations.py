import argparse
import os
import pdb
import sys

import numpy as np
import cv2

import torch

from torch.autograd import Variable
from torch.autograd import Function

from torchvision import transforms

from main import (
    DATASETS,
    HYPER_PARMS,
    IMAGE_NET_PARAMS,
    BiModal,
    get_image_transformer,
    get_loader,
    get_vocabulary,
    pack_padded_sequence,
)

from data_loader import (
    UAVImg,
)

from score_sentence import BASE_PATH

from wwwresults.views import load_assocs_annotated


device = "cpu"


def show_cam_on_image(img, mask):
    heatmap = cv2.applyColorMap(np.uint8(255 * mask), cv2.COLORMAP_JET)
    heatmap = np.float32(heatmap) / 255
    cam = heatmap + np.float32(img)
    cam = cam / np.max(cam)
    cv2.imwrite("cam.jpg", np.uint8(255 * cam))


class MyUAVImg(UAVImg):
    def _load_assocs(self):
        pass


class GradCamModel:
    def __init__(self, model):
        self.model = model
        self.gradients = []

    def save_gradient(self, grad):
        self.gradients.append(grad)

    def get_gradients(self):
        return self.gradients

    def __call__(self, x):
        raise NotImplemented


class GradCamModelBimodal(GradCamModel):
    def __init__(self, model):
        super().__init__(model)
        self.target_layers = ["7"]

    def __call__(self, x, captions, lengths):
        features = []
        encoder = self.model.encoder
        decoder = self.model.decoder
        for name, module in encoder.resnet._modules.items():
            x = module(x)
            if name in self.target_layers:
                x.register_hook(self.save_gradient)
                features += [x]
        x = x.reshape(x.size(0), -1)
        x = encoder.bn(encoder.linear(x))
        x = decoder(x, captions, lengths)
        return features, x


class GradCam:
    def __init__(self, model):
        self.network = GradCamModelBimodal(model)

    def _get_cam(self, features, gradients):
        features = features[-1]
        features = features.cpu().data.numpy()[0, :]

        weights = np.mean(gradients, axis=(2, 3))[0, :]
        cam = np.zeros(features.shape[1:], dtype=np.float32)

        for i, w in enumerate(weights):
            cam += w * features[i, :, :]

        cam = np.maximum(cam, 0)
        cam = cv2.resize(cam, (224, 224))
        cam = cam - np.min(cam)
        cam = cam / np.max(cam)
        return cam

    def __call__(self, inputs, targets):
        features, outputs = self.network(*inputs)
        self.network.model.zero_grad()
        objective = outputs[range(len(targets)), targets].sum()
        objective.backward(retain_graph=True)
        gradients = self.network.get_gradients()[-1].cpu().data.numpy()
        cam = self._get_cam(features, gradients)
        return cam


def get_args():
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    return args


def load_model(n_words):
    MODEL = 'uav-img-32768-1'

    hyper_params = HYPER_PARMS['he']
    vocab_path = os.path.join(BASE_PATH, "data", "vocab_cantab_uav.pkl")

    model = BiModal(
        n_words,
        decoder_dropout_embed=hyper_params["dropout_embed"],
        decoder_dropout_lstm=hyper_params["dropout_lstm"],
    )
    model.load(MODEL)
    model.encoder.to(device)
    model.decoder.to(device)
    model.eval()

    return model


def main():
    # args = get_args()

    vocab_path = os.path.join(BASE_PATH, "data", "vocab_cantab_uav.pkl")
    vocabulary = get_vocabulary(vocab_path)
    transform = get_image_transformer(to_flip=False)

    μ = np.array(IMAGE_NET_PARAMS["mean"])
    σ = np.array(IMAGE_NET_PARAMS["std"])

    inv_normalize = transforms.Normalize(mean=- μ / σ, std=1 / σ)

    model = load_model(len(vocabulary))
    dataset = MyUAVImg("train", 2048, 1, vocabulary, transform)
    loader = get_loader(dataset, batch_size=1, shuffle=False, num_workers=0)

    sentences = load_sentences()

    grad_cam = GradCam(model)

    for i, batch in enumerate(loader):
        images, captions, lengths = batch
        images = images.to(device)
        captions = captions.to(device)
        targets, *_ = pack_padded_sequence(captions, lengths, batch_first=True)
        mask = grad_cam((images, captions, lengths), targets)
        image = inv_normalize(images[0])
        image = image.permute(1, 2, 0)
        show_cam_on_image(image, mask)
        print(sentences[i])
        pdb.set_trace()


if __name__ == "__main__":
    main()
