import os
import pdb
import shutil
import sys

from itertools import groupby

from PIL import Image

import requests

sys.path.append(os.path.expanduser("~/data/uav-commands/image_assoc"))
from assoc import load_assoc


API_URLS = (
    "http://www.image-net.org/api/text/imagenet.synset.geturls.getmapping?wnid={:s}"
)


def swap(tuple1):
    fst, snd = tuple1
    return snd, fst


def get_path(wnid, i):
    root_dir = os.path.expanduser('~/data/imagenet/imgs')
    os.makedirs(os.path.join(root_dir, wnid), exist_ok=True)
    return os.path.join(root_dir, wnid, '{:06d}.JPEG'.format(int(i)))


def wget_img(url, path):
    response = requests.get(url, stream=True, timeout=10)
    with open(path, 'wb') as f:
        shutil.copyfileobj(response.raw, f)
    del response


def load_urls():
    def process(line):
        line = line.strip()
        id1, *url = line.split("\t")
        url = "\t".join(url)  # Sometimes URLs contain tabs
        wnid, i = id1.split("_")
        return wnid, i, url

    with open(
        os.path.expanduser("~/data/imagenet/fall11_urls.txt"),
        "r",
        encoding="ISO-8859-1",
    ) as f:
        lines = (process(line) for line in f.readlines())
        return {k: list(g) for k, g in groupby(lines, key=lambda t: t[0])}


def clean1(path):
    try:
        im = Image.open(path)
    except OSError:
        os.remove(path)
        ok = 0
    else:
        if im.format == 'JPEG':
            pass
            ok = 1
        elif im.format == 'PNG':
            im.convert('RGB').save(path)
            ok = 1
        else:
            os.remove(path)
            ok = 0
    return True


def download_images_wnid(wnid, urls_dict_wnid):

        count = 0
        for _, id1, url in urls_dict_wnid:

            path = get_path(wnid, id1)

            if os.path.exists(path):
                continue

            try:
                wget_img(url, path)
            except Exception as e:
                print(url)
                print('! ERROR')
                print()
                continue

            ok = clean1(path)
            count = count + int(ok)

            print(url)
            print(path)
            print()

            if count >= 2000:
                break


def download_images():
    # get_wnid = lambda url: url.split("=")[1]
    # assoc = load_assoc()
    # wnids = [
    #     (d["name"], get_wnid(d["url"]))
    #     for d in assoc["image-classes"]
    #     if d["name"].startswith("imagenet")
    # ]

    # Download the remaining wnids which were not fetched by the ImageUtils script.
    with open('/tmp/wnids.txt', 'r') as f:
        wnids = [swap(line.strip().split()) for i, line in enumerate(f.readlines()) if i >= 69]

    urls_dict = load_urls()

    for name, wnid in wnids:

        print('→ ', wnid, name)
        print('→ ', len(urls_dict[wnid]))
        print()

        download_images_wnid(wnid, urls_dict[wnid])


def main():
    wnid = sys.argv[1]
    urls_dict = load_urls()
    download_images_wnid(wnid, urls_dict[wnid])
    # download_images()


if __name__ == "__main__":
    main()
