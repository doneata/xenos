import argparse
import os
import pdb
import pickle

from functools import partial

from hyperopt import STATUS_OK, Trials, fmin, hp, tpe

from itertools import cycle

from matplotlib import pyplot as plt

import numpy as np

from pprint import pprint

import torch
import torch.nn as nn

from torchvision import transforms

from torch.nn.utils.rnn import pack_padded_sequence

from torch.optim.lr_scheduler import ReduceLROnPlateau

from build_vocab import Vocabulary, get_vocabulary, get_path

from data_loader import DATASETS, PATH, FlickR, get_loader

from model import EncoderCNN, DecoderRNN

from utils import count_parameters, subsample, symlink_force

from test_vgg import imshow


MODEL_PATH = os.path.join(PATH, "models")
IMAGE_NET_PARAMS = {
    "input_size": 224,
    "mean": [0.485, 0.456, 0.406],
    "std": [0.229, 0.224, 0.225],
}

# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def get_image_transformer(to_flip=True):
    if to_flip:
        transform_flip = [transforms.RandomHorizontalFlip()]
    else:
        transform_flip = []
    return transforms.Compose(
        [
            transforms.Resize(IMAGE_NET_PARAMS["input_size"]),
            transforms.CenterCrop(IMAGE_NET_PARAMS["input_size"]),
        ]
        + transform_flip
        + [
            transforms.ToTensor(),
            transforms.Normalize(IMAGE_NET_PARAMS["mean"], IMAGE_NET_PARAMS["std"]),
        ]
    )


class Model1:
    """Wrapper that unifies the uni- and bi-models."""

    def __init__(
        self,
        num_words,
        embed_size=512,
        hidden_size=512,
        num_layers=2,
        decoder_dropout_embed=0.35,
        decoder_dropout_lstm=0.35,
    ) -> None:
        self.embed_size = embed_size
        self.hidden_size = hidden_size
        self.num_words = num_words
        self.num_layers = num_layers
        self.decoder_dropout_embed = decoder_dropout_embed
        self.decoder_dropout_lstm = decoder_dropout_lstm

    def get_model_path(self, folder, name, i=None):
        if i is None:
            file_name = "{:s}-{:s}.ckpt".format(self.__str__(), name)
        else:
            file_name = "{:s}-{:s}-{:05d}.ckpt".format(self.__str__(), name, i)
        return os.path.join(MODEL_PATH, folder, file_name)

    def zero_grad(self):
        for module in self.modules.values():
            module.zero_grad()

    def train(self):
        for module in self.modules.values():
            module.train()

    def eval(self):
        for module in self.modules.values():
            module.eval()


class UniModal(Model1):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.encoder = lambda b: torch.zeros(len(b), self.embed_size).to(device)
        self.decoder = DecoderRNN(
            self.embed_size,
            self.hidden_size,
            self.num_words,
            self.num_layers,
            dropout_embed=self.decoder_dropout_embed,
            dropout_lstm=self.decoder_dropout_lstm,
        ).to(device)
        self.modules = {"decoder": self.decoder}

    def __str__(self):
        return "unimodal"

    def forward(self, images, captions, lengths):
        return self.decoder(self.encoder(images), captions, lengths)

    def save(self, folder, i: int):
        src = self.get_model_path(folder, "decoder", i)
        dst = self.get_model_path(folder, "decoder")
        torch.save(self.decoder.state_dict(), src)
        symlink_force(os.path.abspath(src), dst)

    def load(self, folder, i: int = None):
        self.decoder.load_state_dict(
            torch.load(self.get_model_path(folder, "decoder", i))
        )

    def params(self):
        return list(self.decoder.parameters())


class BiModal(Model1):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.encoder = EncoderCNN(self.embed_size).to(device)
        self.decoder = DecoderRNN(
            self.embed_size,
            self.hidden_size,
            self.num_words,
            self.num_layers,
            dropout_embed=self.decoder_dropout_embed,
            dropout_lstm=self.decoder_dropout_lstm,
        ).to(device)
        self.modules = {"encoder": self.encoder, "decoder": self.decoder}

    def __str__(self):
        return "bimodal"

    def forward(self, images, captions, lengths):
        return self.decoder(self.encoder(images), captions, lengths)

    def save(self, folder, i: int):
        for m in ["encoder", "decoder"]:
            src = self.get_model_path(folder, m, i)
            dst = self.get_model_path(folder, m)
            torch.save(self.modules[m].state_dict(), src)
            symlink_force(os.path.abspath(src), dst)

    def load(self, folder, i: int = None):
        self.encoder.load_state_dict(
            torch.load(self.get_model_path(folder, "encoder", i))
        )
        self.decoder.load_state_dict(
            torch.load(self.get_model_path(folder, "decoder", i))
        )

    def params(self):
        return (
            list(self.decoder.parameters())
            + list(self.encoder.linear.parameters())
            + list(self.encoder.bn.parameters())
        )


def evaluate1(model, criterion, batch):
    images, captions, lengths = batch

    # Set mini-batch dataset
    images = images.to(device)
    captions = captions.to(device)
    targets = pack_padded_sequence(captions, lengths, batch_first=True)[0]

    # Forward pass
    outputs = model.forward(images, captions, lengths)
    return criterion(outputs, targets)


class BestResult:
    def __init__(self):
        self.best_loss = np.inf

    def is_best(self, loss):
        return loss < self.best_loss

    def update(self, loss):
        self.best_loss = min(loss, self.best_loss)


class LoggerCallback:
    def __init__(self):
        self.HEADER = "{:>5s} | {:>10s} | {:>10s} | {:>10s} | {:>10s} | {:10s} "
        self.ROW = "{:5d} | {:10.4f} | {:10.4f} | {:10.4f} | {:10.4f} | {:s} "

    def __call__(self, step, loss_train, loss_valid, is_best, **kwargs):
        if step == 0:
            print(
                self.HEADER.format(
                    "step", "train loss", "train ppl", "valid loss", "valid ppl", "best"
                )
            )
        best_str = "←" if is_best else "."
        print(
            self.ROW.format(
                step,
                loss_train,
                np.exp(loss_train),
                loss_valid,
                np.exp(loss_valid),
                best_str,
            )
        )


class EarlyStopping:
    def __init__(self, nr_tries):
        self.no_improv = 0
        self.nr_tries = nr_tries

    def __call__(self, is_best, **kwargs):
        if self.no_improv >= self.nr_tries:
            raise StopIteration
        if is_best:
            self.no_improv = 0
        else:
            self.no_improv = self.no_improv + 1


class FixedStopping:
    def __init__(self, nr_iterations):
        self.nr_iterations = nr_iterations

    def __call__(self, step, **kwargs):
        if step >= self.nr_iterations:
            raise StopIteration


class ModelCheckpoint:
    def __init__(self, folder):
        os.makedirs(os.path.join(MODEL_PATH, folder), exist_ok=True)
        self.folder = folder

    def __call__(self, model, step, is_best, **kwargs):
        if is_best:
            model.save(self.folder, step)


class LearningRateScheduler:
    def __init__(self, optimizer, patience):
        self.scheduler = ReduceLROnPlateau(
            optimizer, patience=patience, verbose=True, factor=0.25
        )

    def __call__(self, loss_valid, **kwargs):
        self.scheduler.step(loss_valid)


def train(model, get_loader, optimizer, is_checkpoint, callbacks=tuple()):
    # Build data loader
    data_loader_train = get_loader("train")
    data_loader_valid = get_loader("valid")

    # Loss
    criterion = nn.CrossEntropyLoss()
    best_result = BestResult()

    for i, batch in enumerate(cycle(data_loader_train)):

        model.train()
        loss = evaluate1(model, criterion, batch)

        model.zero_grad()
        loss.backward()
        optimizer.step()

        if is_checkpoint(i):

            model.eval()
            losses = [evaluate1(model, criterion, b).data for b in data_loader_valid]

            loss_valid = np.mean(losses)
            loss_train = loss.data

            state = {
                "step": i,
                "loss_train": loss_train,
                "loss_valid": loss_valid,
                "is_best": best_result.is_best(loss_valid),
                "model": model,
            }
            best_result.update(loss_valid)

            try:
                for callback in callbacks:
                    callback(**state)
            except StopIteration:
                break

    return {"loss": best_result.best_loss, "status": STATUS_OK}


def evaluate(model, get_loader):
    data_loader = get_loader("test")
    criterion = nn.CrossEntropyLoss()
    model.eval()
    losses = [evaluate1(model, criterion, b).data for b in data_loader]
    loss = np.mean(losses)

    HEADER = "{:>10s} | {:>10s}"
    ROW = "{:10.4f} | {:10.4f}"
    print(HEADER.format("test loss", "test ppl"))
    print(ROW.format(loss, np.exp(loss)))


def sample(model, get_loader):

    data_loader = get_loader("test")
    idx2word = data_loader.dataset.vocabulary.idx2word
    to_words = lambda tensor: [idx2word[i] for i in tensor.data.tolist()]

    for images, captions_ids_gt, lengths in data_loader:

        images = images.to(device)
        captions_ids = model.decoder.sample(model.encoder(images))

        for image, caption_ids, caption_ids_gt, length in zip(
            images, captions_ids, captions_ids_gt, lengths
        ):

            imshow(image)

            caption_groundtruth = to_words(caption_ids_gt[:length])
            caption_generated = to_words(caption_ids)

            print("→", " ".join(caption_groundtruth))
            print("?", " ".join(caption_generated))
            print()

            plt.show()


# Named after chemical elements
HYPER_PARMS = {
    # Hydrogen
    "h": {"learning_rate": 0.001, "dropout_embed": 0.35, "dropout_lstm": 0.35},
    # Helium
    # Obtained by hyper-parameter tuning
    "he": {"learning_rate": 0.002, "dropout_embed": 0.081, "dropout_lstm": 0.648},
    # Lithium
    # Same as Helium, but with smaller learning rate
    # Used when "--init" is specified
    "li": {"learning_rate": 0.001, "dropout_embed": 0.081, "dropout_lstm": 0.648},
}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--todo",
        type=str,
        choices=["train", "train-opt", "evaluate", "sample"],
        nargs="+",
        required=True,
        help="choose the task to do",
    )
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        choices=["uni", "bi"],
        required=True,
        help="which model to use: uni-modal (text-only) or bi-modal (text and images)",
    )
    parser.add_argument(
        "-d",
        "--dataset",
        type=str,
        choices=DATASETS,
        required=True,
        help="choose a dataset",
    )
    parser.add_argument(
        "--checkpoint",
        type=int,
        default=100,
        help="number of steps between checkpoints",
    )
    parser.add_argument(
        "--hyper-params",
        choices=HYPER_PARMS,
        default="h",
        help="choice of hyper-parameters",
    )
    parser.add_argument(
        "--init",
        help="name of dataset used to train model for initialization (this option is relevant only when 'todo' is 'train')",
    )
    parser.add_argument("--vocab-path", help="path to vocabulary")
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--num_workers", type=int, default=0)

    args = parser.parse_args()
    print(args)

    if args.dataset == "ptb":
        assert args.model == "uni", "PTB is a text-only dataset"

    Dataset = DATASETS[args.dataset]
    hyper_params = HYPER_PARMS[args.hyper_params]

    if args.vocab_path:
        vocab_path = args.vocab_path
    elif args.dataset == "cantab":
        vocab_path = os.path.join("data", "vocab_cantab_uav.pkl")
    elif args.dataset == "ted-lium":
        vocab_path = os.path.join("data", "vocab_ted-lium_topk_10000.pkl")
    else:
        vocab_path = get_path(args.dataset, 0)

    vocabulary = get_vocabulary(vocab_path)
    transform = get_image_transformer()

    get_loader1 = lambda split: get_loader(
        dataset=Dataset(split, vocabulary=vocabulary, transform=transform),
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=args.num_workers,
    )

    if args.model == "uni":
        Model = UniModal
    elif args.model == "bi":
        Model = BiModal
    else:
        assert False, "Unknown model"

    num_words = len(vocabulary)
    model = Model(
        num_words,
        decoder_dropout_embed=hyper_params["dropout_embed"],
        decoder_dropout_lstm=hyper_params["dropout_lstm"],
    )
    print(
        "Number of parameters for decoder: {:,}".format(count_parameters(model.decoder))
    )

    if "train" in args.todo:

        print("Chosen hyper-parameters:")
        pprint(hyper_params)

        if args.init:
            p = os.path.join("models", args.init, "unimodal-decoder.ckpt")
            model.decoder.load_state_dict(torch.load(p))
            model_out_dir = args.dataset + "-init-" + args.init
        else:
            model_out_dir = args.dataset

        is_checkpoint = lambda i: i % 100 == 0
        optimizer = torch.optim.Adam(model.params(), lr=hyper_params["learning_rate"])

        # Callbacks
        logger = LoggerCallback()
        early_stopper = EarlyStopping(nr_tries=9)
        model_checkpointer = ModelCheckpoint(model_out_dir)
        scheduler = LearningRateScheduler(optimizer, 3)

        train(
            model,
            get_loader1,
            optimizer,
            is_checkpoint=is_checkpoint,
            callbacks=(logger, early_stopper, scheduler, model_checkpointer),
        )
    elif "train-opt" in args.todo:
        is_checkpoint = lambda i: i % 100 == 0

        def objective(kwargs):
            pprint(kwargs)
            logger = LoggerCallback()
            early_stopper = EarlyStopping(nr_tries=3)
            model = Model(
                num_words,
                decoder_dropout_embed=kwargs["dropout_embed"],
                decoder_dropout_lstm=kwargs["dropout_lstm"],
            )
            optimizer = torch.optim.Adam(model.params(), lr=kwargs["learning_rate"])
            status = train(
                model,
                get_loader1,
                optimizer,
                is_checkpoint=is_checkpoint,
                callbacks=(logger, early_stopper),
            )
            print()
            print("Hyper-parameters :")
            for k, v in kwargs.items():
                print("{:16s} : {}".format(k, v))
            print("{:16s} : {}".format("loss", status["loss"]))
            return status

        trials = Trials()
        best = fmin(
            objective,
            space={
                "learning_rate": hp.loguniform(
                    "learning_rate", low=np.log(0.0003), high=np.log(0.03)
                ),
                "dropout_embed": hp.uniform("dropout_embed", low=0.0, high=1.0),
                "dropout_lstm": hp.uniform("dropout_lstm", low=0.0, high=1.0),
            },
            max_evals=64,
            algo=tpe.suggest,
            trials=trials,
        )
        print(best)
    elif "evaluate" in args.todo:
        model.load(args.dataset)
        evaluate(model, get_loader1)
    elif "sample" in args.todo:
        model.load(args.dataset)
        sample(model, get_loader1)


if __name__ == "__main__":
    main()
