import argparse
import json
import os
import pdb
import pickle
import random
import sys

from itertools import (groupby, product)

import numpy as np
import nltk

from PIL import Image

from pprint import pprint

import torch
import torchvision.transforms as transforms
import torch.utils.data as data

from torch.utils.data import Dataset

import build_vocab

from typing import List, Tuple

from utils import flatten

# For the `UAVImg` dataset
sys.path.append(os.path.expanduser('~/data/uav-commands/image_assoc'))
from assoc import load_assoc, match


PATH = os.path.expanduser('~/work/xenos')
ROOT_DIR = "/home/doneata/data"


class PTB(Dataset):
    def __init__(self, split, vocabulary=None, **kwargs):
        assert split in ["train", "valid", "test"], "Unknown split"
        self.split = split
        self.sentences = self.load_sentences()
        self.vocabulary = vocabulary

    def load_sentences(self):
        path = os.path.join(
            ROOT_DIR, "penn-tree-bank", "ptb.{:s}.txt".format(self.split)
        )
        with open(path, "r") as f:
            return [line.strip() for line in f.readlines()]

    def __str__(self):
        return "ptb"

    def __len__(self):
        return len(self.sentences)

    def __getitem__(self, index):
        sentence = self.sentences[index]
        words = sentence.split()
        if self.vocabulary:
            words = (
                [self.vocabulary.TOKENS["START"]]
                + words
                + [self.vocabulary.TOKENS["END"]]
            )
            words = torch.Tensor([self.vocabulary(word) for word in words])
        blank = torch.zeros(1)
        return blank, words


class TextDataset(Dataset):
    def __init__(self, vocabulary=None, **kwargs):
        self.sentences = self.load_sentences()
        self.vocabulary = vocabulary

    def __len__(self):
        return len(self.sentences)

    def __getitem__(self, index):
        sentence = self.sentences[index]
        words = sentence.split()
        if self.vocabulary:
            words = (
                [self.vocabulary.TOKENS["START"]]
                + words
                + [self.vocabulary.TOKENS["END"]]
            )
            words = torch.Tensor([self.vocabulary(word) for word in words])
        blank = torch.zeros(1)
        return blank, words

    def _drop_id(self, s):
        _, *words = s.split()
        return " ".join(words)


class Cantab(TextDataset):
    def __init__(self, split, **kwargs):
        self.RENAME_SPLIT = {"train": "train", "valid": "dev"}
        assert split in self.RENAME_SPLIT, "Unknown split"
        self.split = split
        self.root_dir = (
            "/home/doneata/work/experiments-tedlium-r2/data/rnnlm/text_nosp/"
        )
        super().__init__(**kwargs)

    def load_sentences(self):
        split = self.RENAME_SPLIT[self.split]
        path = os.path.join(self.root_dir, "{:s}.txt".format(split))
        with open(path, "r") as f:
            return [line.strip() for line in f.readlines()]

    def __str__(self):
        return "cantab"


class UAV(TextDataset):
    def __init__(self, split, **kwargs):
        assert split in ["test"], "Unknown split"
        self.split = split
        super().__init__(**kwargs)

    def load_sentences(self):
        path = os.path.join(ROOT_DIR, "uav-commands", "commands")

        def load_speaker(file1):
            with open(os.path.join(path, file1), "r") as f:
                return [self._drop_id(line.strip()) for line in f.readlines()]

        return flatten(load_speaker(p) for p in os.listdir(path))

    def __str__(self):
        return "uav"


class TEDLIUM(TextDataset):
    def __init__(self, split, **kwargs):
        self.RENAME_SPLIT = {"train": "train", "valid": "dev"}
        assert split in self.RENAME_SPLIT, "Unknown split"
        self.split = split
        self.root_dir = "/home/doneata/work/experiments-tedlium-r2"
        super().__init__(**kwargs)

    def load_sentences(self):
        split = self.RENAME_SPLIT[self.split]
        path = os.path.join(self.root_dir, "data", split, "text")
        with open(path, "r") as f:
            return [self._drop_id(line.strip()) for line in f.readlines()]

    def __str__(self):
        return "ted-lium"


class FlickR(Dataset):
    def __init__(self, split, vocabulary=None, transform=None):

        self.RENAME_SPLIT = {"train": "train", "valid": "dev", "test": "test"}
        assert split in self.RENAME_SPLIT, "Unknown split"

        self.split = split
        self.vocabulary = vocabulary
        self.transform = transform

        self.img_dir = os.path.join(ROOT_DIR, "flickr8k-images")
        self.txt_dir = os.path.join(ROOT_DIR, "flickr8k-text")

        self.image_names = self.load_image_names()
        self.captions = self.load_captions()

    def load_image_names(self):
        split = self.RENAME_SPLIT[self.split]
        path = os.path.join(self.txt_dir, "Flickr_8k.{:s}Images.txt".format(split))
        with open(path, "r") as f:
            return [line.strip() for line in f.readlines()]

    def load_captions(self) -> List[Tuple[str, str]]:
        def process(line):
            key, caption = line.strip().split("\t")
            id_, n = key.split("#")
            return (id_, int(n), caption.lower())

        by_id = lambda tup: tup[0]
        path = os.path.join(self.txt_dir, "Flickr8k.token.txt")

        with open(path, "r") as f:
            lines = f.readlines()

        captions = [process(line) for line in lines]
        captions = groupby(sorted(captions, key=by_id), key=by_id)
        captions = [(k, c) for k, group in captions for _, _, c in group]

        image_names = set(self.image_names)
        captions = [(k, c) for k, c in captions if k in image_names]

        return captions

    def load_sentences(self):
        return [c for k, c in self.load_captions()]

    def __str__(self):
        return "flickr"

    def __len__(self):
        return len(self.captions)

    def __getitem__(self, index):
        image_name, caption = self.captions[index]
        image = Image.open(os.path.join(self.img_dir, image_name))
        words = caption.split()
        if self.transform:
            image = self.transform(image)
        if self.vocabulary:
            words = (
                [self.vocabulary.TOKENS["START"]]
                + caption.split()
                + [self.vocabulary.TOKENS["END"]]
            )
            words = torch.Tensor([self.vocabulary(word) for word in words])
        return image, words


class UAVText(TextDataset):
    def __init__(self, split, size, fold, transform=None, **kwargs):
        assert split in ["train", ], "Unknown split"
        self.split = split
        self.key = "{:d}_{:d}".format(size, fold)
        self.transform = transform
        super().__init__(**kwargs)

    def load_sentences(self):
        path = os.path.join(ROOT_DIR, "uav-commands", "domain_adaptation", "commands", self.key + ".txt")
        with open(path, "r") as f:
            return [line.strip() for line in f.readlines()]

    def __str__(self):
        return "uav-text-" + self.key.replace("_", "-")


class UAVImg(Dataset):
    def __init__(self, split, size, fold, vocabulary=None, transform=None):
        assert split in ["train", ], "Unknown split"

        self.split = split
        self.vocabulary = vocabulary
        self.transform = transform

        self.key = "{:d}_{:d}".format(size, fold)
        self.assocs = self._load_assocs()

    def load_sentences(self):
        return [assoc["command"] for assoc in self.assocs]

    def _load_assocs(self):
        path = os.path.join(ROOT_DIR, "uav-commands", "domain_adaptation", "assoc", self.key + ".json")
        with open(path, 'r') as f:
            return json.load(f)

    def __getitem__(self, index):
        assoc = self.assocs[index]

        caption = assoc["command"]
        image_name = assoc["image-path"]

        words = caption.split()
        image = Image.open(image_name)

        if image.mode != "RGB":
            image = image.convert("RGB")

        if self.transform:
            image = self.transform(image)

        if self.vocabulary:
            words = (
                [self.vocabulary.TOKENS["START"]]
                + caption.split()
                + [self.vocabulary.TOKENS["END"]]
            )
            words = torch.Tensor([self.vocabulary(word) for word in words])

        return image, words

    def __len__(self):
        return len(self.assocs)

    def __str__(self):
        return "uav-img-" + self.key.replace("_", "-")


class CocoDataset(data.Dataset):
    """COCO Custom Dataset compatible with torch.utils.data.DataLoader."""

    # from pycocotools.coco import COCO

    def __init__(self, root, json, vocab, transform=None):
        """Set the path for images, captions and vocabulary wrapper.
        Args:
            root: image directory.
            json: coco annotation file path.
            vocab: vocabulary wrapper.
            transform: image transformer.
        """
        self.root = root
        self.coco = COCO(json)
        self.ids = list(self.coco.anns.keys())
        self.vocab = vocab
        self.transform = transform

    def __getitem__(self, index):
        """Returns one data pair (image and caption)."""
        coco = self.coco
        vocab = self.vocab
        ann_id = self.ids[index]
        caption = coco.anns[ann_id]["caption"]
        img_id = coco.anns[ann_id]["image_id"]
        path = coco.loadImgs(img_id)[0]["file_name"]

        image = Image.open(os.path.join(self.root, path)).convert("RGB")
        if self.transform is not None:
            image = self.transform(image)

        # Convert caption (string) to word ids.
        tokens = nltk.tokenize.word_tokenize(str(caption).lower())
        caption = []
        caption.append(vocab("<start>"))
        caption.extend([vocab(token) for token in tokens])
        caption.append(vocab("<end>"))
        target = torch.Tensor(caption)
        return image, target

    def __len__(self):
        return len(self.ids)


def collate_fn(data):
    """Creates mini-batch tensors from the list of tuples (image, caption).

    We should build custom collate_fn rather than using default collate_fn,
    because merging caption (including padding) is not supported in default.

    Args:
        data: list of tuple (image, caption).
            - image: torch tensor of shape (3, 256, 256).
            - caption: torch tensor of shape (?); variable length.

    Returns:
        images: torch tensor of shape (batch_size, 3, 256, 256).
        targets: torch tensor of shape (batch_size, padded_length).
        lengths: list; valid length for each padded caption.

    """
    # Sort a data list by caption length (descending order).
    data.sort(key=lambda x: len(x[1]), reverse=True)
    images, captions = zip(*data)

    # Merge images (from tuple of 3D tensor to 4D tensor).
    images = torch.stack(images, 0)

    # Merge captions (from tuple of 1D tensor to 2D tensor).
    lengths = [len(cap) for cap in captions]
    targets = torch.zeros(len(captions), max(lengths)).long()
    for i, cap in enumerate(captions):
        end = lengths[i]
        targets[i, :end] = cap[:end]
    return images, targets, lengths


def collate_fn_2(data):
    """Creates mini-batch tensors from the list of tuples (image, caption).

    We should build custom collate_fn rather than using default collate_fn,
    because merging caption (including padding) is not supported in default.

    Args:
        data: list of tuple (image, caption).
            - image: torch tensor of shape (3, 256, 256).
            - caption: torch tensor of shape (?); variable length.

    Returns:
        images: torch tensor of shape (batch_size, 3, 256, 256).
        targets: torch tensor of shape (batch_size, padded_length).
        lengths: list; valid length for each padded caption.

    """
    images, captions = zip(*data)

    # Sort by caption length (descending order).
    lengths = torch.Tensor(list(map(len, captions))).long()
    lengths, indices = torch.sort(lengths, descending=True)
    max_len = max(lengths)

    # Merge images (from tuple of 3D tensor to 4D tensor).
    images = torch.stack(images, 0)

    # Merge captions (from tuple of 1D tensor to 2D tensor).
    targets = torch.zeros(len(captions), max_len).long()
    for i, caption in enumerate(captions):
        end = len(caption)
        targets[i, :end] = caption[:end]

    images = images[indices]
    targets = targets[indices]

    return images, targets, lengths, indices


def get_loader(dataset, batch_size, shuffle, num_workers, collate_function=None):
    # The data loader will return (images, captions, lengths) for each iteration.
    # images: a tensor of shape (batch_size, 3, 224, 224).
    # captions: a tensor of shape (batch_size, padded_length).
    # lengths: a list indicating valid length for each caption; its length is (batch_size).
    collate_function = collate_function or collate_fn
    return torch.utils.data.DataLoader(
        dataset=dataset,
        batch_size=batch_size,
        shuffle=shuffle,
        num_workers=num_workers,
        collate_fn=collate_function,
    )


UAV_TYPES = {"text": UAVText, "img": UAVImg}
UAV_SIZES = [2 ** i for i in range(11, 17)]
UAV_FOLDS = list(range(1, 6))

def create_uav_dataset(t, s, f):
    return lambda split, **kwargs: UAV_TYPES[t](split, s, f, **kwargs)

DATASETS = {"cantab": Cantab, "flickr": FlickR, "ptb": PTB, "ted-lium": TEDLIUM}
for t, s, f in product(UAV_TYPES, UAV_SIZES, UAV_FOLDS):
    key = "uav-{}-{}-{}".format(t, s, f)
    DATASETS[key] = create_uav_dataset(t, s, f)


def main():

    parser = argparse.ArgumentParser("View a sample of a dataset.")
    parser.add_argument(
        "-d",
        "--dataset",
        type=str,
        choices=DATASETS,
        required=True,
        help="choose a dataset",
    )
    args = parser.parse_args()
    print(args)

    dataset = DATASETS[args.dataset](split="train")
    for _ in range(10):
        i = random.choice(range(len(dataset)))
        _, words = dataset[i]
        print("{:6d} → {:s}".format(i, " ".join(words)))


if __name__ == "__main__":
    main()
