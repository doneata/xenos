import argparse
import csv
import json
import os
import pdb

from PIL import Image

import torch

from torch.nn.utils.rnn import PackedSequence, pad_packed_sequence

from torch.nn.functional import log_softmax

from main import (
    DATASETS,
    HYPER_PARMS,
    IMAGE_NET_PARAMS,
    BiModal,
    UniModal,
    device,
    get_image_transformer,
    get_loader,
    get_vocabulary,
    pack_padded_sequence,
)

from data_loader import Dataset, TextDataset, collate_fn_2

from wwwgallery.views import grouper


BASE_PATH = "/home/doneata/work/xenos"
ROOT_DIR = "/home/doneata/data/uav-commands"
INPUT_SIZE = IMAGE_NET_PARAMS["input_size"]


def parse_line(line, to_return_id):
    id1, *words = line.strip().split()
    sentence = " ".join(words)
    if to_return_id:
        return id1, sentence
    else:
        return sentence


def load_sentences(path, to_return_id=False):
    with open(path, "r") as f:
        return [parse_line(line, to_return_id) for line in f.readlines()]


def load_image_command_test_assocs():
    path = os.path.join(ROOT_DIR, "domain_adaptation", "assoc", "test.json")
    with open(path, "r") as f:
        json_data = json.load(f)
    # Index by id
    return {datum["id"]: datum for datum in json_data}


class DatasetText(TextDataset):
    def __init__(self, path, **kwargs):
        self.path = path
        super().__init__(**kwargs)

    def load_sentences(self, to_return_id=False):
        return load_sentences(self.path, to_return_id)


class DatasetImg(Dataset):
    def __init__(self, path, assoc_type="annotated", vocabulary=None, transform=None):
        self.vocabulary = vocabulary
        self.transform = transform
        self.sentences = load_sentences(path, to_return_id=True)

        if assoc_type == 'annotated':
            self._get_image_name = self._get_image_name_annotated
        elif assoc_type == 'generated':
            self.assocs = load_image_command_test_assocs()
            self._get_image_name = self._get_image_name_generated
        elif assoc_type == 'black':
            def raise_error(*args):
                raise FileNotFoundError
            self._get_image_name = raise_error

    def _get_command_id(self, key):
        key, _ = key.split('-')
        _, _, id_ = key.split('_')
        return id_

    def _get_image_name_annotated(self, id_):
        return os.path.join(ROOT_DIR, "images", "{:04d}.jpg".format(int(id_)))

    def _get_image_name_generated(self, id_):
        return self.assocs[id_]["image-path"]

    def __getitem__(self, index):
        key, caption = self.sentences[index]
        id_ = self._get_command_id(key)

        image_path = self._get_image_name(id_)
        image = Image.open(image_path)
        # try:
        # except (KeyError, FileNotFoundError):
        #     print("WARN", id_)
        #     image = Image.new("RGB", (INPUT_SIZE, INPUT_SIZE))

        if image.mode != "RGB":
            image = image.convert("RGB")

        if self.transform:
            image = self.transform(image)

        words = caption.split()

        if self.vocabulary:
            words = (
                [self.vocabulary.TOKENS["START"]]
                + caption.split()
                + [self.vocabulary.TOKENS["END"]]
            )
            words = torch.Tensor([self.vocabulary(word) for word in words])

        return image, words

    def __len__(self):
        return len(self.sentences)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--dataset",
        type=str,
        required=True,
        help="choose the dataset on which the model was trained",
    )
    parser.add_argument(
        "-i", "--path-in", type=str, required=True, help="path to words text"
    )
    parser.add_argument(
        "-o", "--path-out", type=str, required=True, help="path to score file"
    )
    parser.add_argument(
        "--assoc-type", choices=("black", "annotated", "generated"), default="annotated", help="how the images are associated to the test commands"
    )
    parser.add_argument("-v", "--verbose", default=0, action="count", help="verbosity")
    args = parser.parse_args()

    assert args.dataset.startswith('uav-text') or args.dataset.startswith('uav-img'), "Unknown dataset"

    h = "he"
    BATCH_SIZE = 128

    hyper_params = HYPER_PARMS[h]
    vocab_path = os.path.join(BASE_PATH, "data", "vocab_cantab_uav.pkl")

    vocabulary = get_vocabulary(vocab_path)
    transform = get_image_transformer(to_flip=False)

    if args.dataset.startswith("uav-text"):
        Model = UniModal
        Dataset = DatasetText
    elif args.dataset.startswith("uav-img"):
        Model = BiModal
        Dataset = lambda p, **kwargs: DatasetImg(p, assoc_type=args.assoc_type, **kwargs)

    dataset = Dataset(args.path_in, vocabulary=vocabulary, transform=transform)

    get_loader1 = lambda split: get_loader(
        dataset=dataset,
        batch_size=BATCH_SIZE,
        shuffle=False,
        num_workers=0,
        collate_function=collate_fn_2,
    )

    loader = get_loader1("train")

    num_words = len(vocabulary)
    model = Model(
        num_words,
        decoder_dropout_embed=hyper_params["dropout_embed"],
        decoder_dropout_lstm=hyper_params["dropout_lstm"],
    )
    model.load(args.dataset)
    model.eval()

    sentences = load_sentences(args.path_in, to_return_id=True)
    sentences_group = grouper(BATCH_SIZE, sentences)

    UNK = vocabulary.word2idx["<unk>"]
    PROB_UNK = 1 / 257973  # Number of words in CANTAB
    # TODO Assert that the vocabulary comes indeed from CANTAB

    results = []

    for i, batch in enumerate(loader):
        images, captions, lengths, indices = batch

        # Set mini-batch dataset
        images = images.to(device)
        captions = captions.to(device)
        targets, batch_sizes = pack_padded_sequence(captions, lengths, batch_first=True)

        # Forward pass
        outputs = model.forward(images, captions, lengths)
        softmax = log_softmax(outputs, 1)[range(len(targets)), targets]

        ix = targets == UNK
        softmax[ix] = (softmax[ix].exp() * PROB_UNK).log()

        # Undo padding
        softmax, _ = pad_packed_sequence(
            PackedSequence(softmax, batch_sizes), batch_first=True
        )

        # Undo sorting
        _, undo_indices = indices.sort(0)
        softmax = softmax[undo_indices]

        # --
        softmax = softmax.sum(1)

        # lengths_unsorted = lengths[orig_indices]
        # unpacked_out_unsorted = unpacked_out.argmax(2)[orig_indices]

        group = next(sentences_group)

        for (id1, sentence), score in zip(group, softmax):

            score = score.tolist()
            results.append((id1, -score))

            if args.verbose == 1:
                # Use a similar format to what is used by Kaldi.
                print("{} {:.4f}".format(id1, -score))
            elif args.verbose >= 2:
                print("{} {:+.2f} {}".format(id1, score, sentence))

    with open(args.path_out, "w") as f:
        for id1, score in results:
            f.write("{} {:.4f}\n".format(id1, score))


if __name__ == "__main__":
    main()
