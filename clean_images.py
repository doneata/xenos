"""Removes images from ImageNet that have various issues:

- partially downloaded and cannot be opened by PIL
- empty template from FlickR

"""
import argparse
import os
import pdb
import sys

from toolz import compose

from PIL import Image


BASE_PATH = "/home/doneata/data/imagenet/imgs"


def is_truncated(path):
    im = Image.open(path)
    try:
        im.load()
    except OSError as e:
        return True
    return False


def is_empty_flickr(path):
    return os.path.getsize(path) == 7115


FUNCS = {
    'truncated': is_truncated,
    'empty': is_empty_flickr,
}


def main():
    funcs = ['empty', ]
    funcs = [FUNCS[f] for f in funcs]
    has_issues = lambda path: any(func(path) for func in funcs)
    for root, dirs, files in os.walk(BASE_PATH):
        for f in files:
            path = os.path.join(root, f)

            sys.stdout.write(path + "\r")
            sys.stdout.flush()

            if has_issues(path):
                print()
                os.rename(path, os.path.join('/tmp/imagenet', f))


if __name__ == "__main__":
    main()
