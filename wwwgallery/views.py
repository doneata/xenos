import csv
import json
import os
import pdb

from itertools import zip_longest

from flask import render_template

from . import app


BASE = "/home/doneata/data"


def grouper(n, iterable, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)


def load_test():
    name = "uav-command-image-assoc.csv"
    path = os.path.join(BASE, "uav-commands", name)

    def to_dict(row):
        _, annotator, command_id, command, *_ = row
        _, id_ = command_id.split("_")
        path = "uav-commands/images/{:04d}.jpg".format(int(id_))
        if os.path.exists(os.path.join(BASE, path)):
            path = "/static/data/" + path
        else:
            path = 'http://placehold.it/256x256'
        return {
            "command": command,
            "annotator": annotator,
            "image-path": path,
        }
    with open(path, 'r') as f:
        reader = csv.reader(f, delimiter=",")
        _ = next(reader)
        data = [to_dict(row) for row in reader]

    return data


def load_assoc(size, fold):
    name = "{}_{}.json".format(size, fold)
    path = os.path.join(BASE, "uav-commands", "domain_adaptation", "assoc", name)
    with open(path, "r") as f:
        return json.load(f)


@app.route('/')
def index():
    return render_template("index.html", title="UAV Dataset")


@app.route('/fold/<int:fold>')
def train(fold):
    size = 2048
    assoc = load_assoc(size, fold)
    for elem in assoc:
        if elem["image-path"]:
            elem["not-available"] = os.path.getsize(elem["image-path"]) == 7115
            elem["image-path"] = elem["image-path"].replace('/home/doneata', '/static')
        else:
            elem["image-path"] = 'http://placehold.it/256x256'
    return render_template(
        "train.html",
        title="UAV Dataset",
        size=size,
        fold=fold,
        assoc=grouper(4, assoc),
    )


@app.route('/test')
def test():
     data = load_test()
     return render_template(
        "test.html",
        title="UAV Dataset",
        data=grouper(4, data),
    )
