import argparse
import pdb
import os

from itertools import cycle

from pprint import pprint

import numpy as np

import torch
import torch.nn as nn

from utils import count_parameters

from main import (
    DATASETS,
    HYPER_PARMS,
    MODEL_PATH,
    STATUS_OK,
    BiModal,
    LoggerCallback,
    UniModal,
    evaluate1,
    get_image_transformer,
    get_loader,
    get_path,
    get_vocabulary,
)


class StopperCallback:
    def __init__(self, nr_steps, folder):
        self.folder = folder
        self.nr_steps = nr_steps
        self.step = 0
        # Create folder if it doesn't exist.
        os.makedirs(os.path.join(MODEL_PATH, self.folder), exist_ok=True)
        # How often to save the intermediate results
        self.FREQ = 15

    def __call__(self, step, model, **kwargs):
        if self.step > 0 and self.step % self.FREQ == 0:
            model.save(self.folder, self.step)
        if self.step >= self.nr_steps:
            model.save(self.folder, self.step)
            raise StopIteration
        self.step = self.step + 1


def train(model, get_loader, optimizer, is_checkpoint, callbacks=tuple()):

    # Build data loader
    data_loader_train = get_loader("train")

    # Loss
    criterion = nn.CrossEntropyLoss()

    for i, batch in enumerate(cycle(data_loader_train)):

        model.train()
        loss = evaluate1(model, criterion, batch)

        model.zero_grad()
        loss.backward()
        optimizer.step()

        if is_checkpoint(i):

            model.eval()
            loss_train = loss.data

            state = {
                "step": i,
                "loss_train": loss_train,
                "loss_valid": np.nan,
                "is_best": None,
                "model": model,
            }

            try:
                for callback in callbacks:
                    callback(**state)
            except StopIteration:
                break

    return {"loss": loss.data, "status": STATUS_OK}


#  def get_folder_path(args):
#      return "-".join((args.dataset, args.dataset_source))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        choices=["uni", "bi"],
        required=True,
        help="which model to use: uni-modal (text-only) or bi-modal (text and images)",
    )
    parser.add_argument(
        "-d",
        "--dataset",
        type=str,
        choices=DATASETS,
        required=True,
        help="choose a dataset to fine-tune on",
    )
    parser.add_argument(
        "--dataset-source",
        type=str,
        choices=["cantab", "flickr-init-cantab"],
        required=True,
        help="source dataset on which the initial model was trained",
    )
    parser.add_argument(
        "--nr-steps",
        type=int,
        default=15,
        help="source dataset on which the initial model was trained",
    )
    parser.add_argument(
        "--vocab-path",
        help="path to vocabulary",
    )
    args = parser.parse_args()
    print(args)

    H = "he"
    BATCH_SIZE = 128
    NUM_WORKERS = 0

    NR_STEPS = 15
    LEARNING_RATE = 0.0005

    if args.dataset.startswith('uav'):
        _, _, n, _ = args.dataset.split('-')
        NR_STEPS = int(n) / 2 ** 10
        NR_STEPS = int(NR_STEPS)
        print('→ Updated NR_STEPS to', NR_STEPS)

    if args.dataset == "ptb":
        assert args.model == "uni", "PTB is a text-only dataset"

    Dataset = DATASETS[args.dataset]
    hyper_params = HYPER_PARMS[H]

    if args.vocab_path:
        vocab_path = args.vocab_path
    elif args.dataset_source == "cantab":
        vocab_path = os.path.join("data", "vocab_cantab_uav.pkl")
    elif args.dataset_source == "ted-lium":
        vocab_path = os.path.join("data", "vocab_ted-lium_topk_10000.pkl")
    else:
        vocab_path = get_path(args.dataset_source, 0)

    vocabulary = get_vocabulary(vocab_path)
    transform = get_image_transformer()

    get_loader1 = lambda split: get_loader(
        dataset=Dataset(split, vocabulary=vocabulary, transform=transform),
        batch_size=BATCH_SIZE,
        shuffle=True,
        num_workers=NUM_WORKERS,
    )

    if args.model == "uni":
        Model = UniModal
    elif args.model == "bi":
        Model = BiModal
    else:
        assert False, "Unknown model"

    num_words = len(vocabulary)
    model = Model(
        num_words,
        decoder_dropout_embed=hyper_params["dropout_embed"],
        decoder_dropout_lstm=hyper_params["dropout_lstm"],
    )
    print(
        "Number of parameters for decoder: {:,}".format(count_parameters(model.decoder))
    )

    print("Chosen hyper-parameters:")
    pprint(hyper_params)

    is_checkpoint = lambda i: i % 100 == 0
    optimizer = torch.optim.Adam(model.params(), lr=LEARNING_RATE)

    logger = LoggerCallback()
    stopper = StopperCallback(nr_steps=NR_STEPS, folder=args.dataset)

    model.load(args.dataset_source)
    train(
        model,
        get_loader1,
        optimizer,
        is_checkpoint=is_checkpoint,
        callbacks=(logger, stopper),
    )


if __name__ == "__main__":
    main()
