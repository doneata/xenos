import argparse
import csv
import json
import os
import pdb
import random
import sys

from collections import Counter

from functools import partial

from itertools import product

from pprint import pprint as pp

from data_loader import UAV_FOLDS as FOLDS, UAV_SIZES as SIZES, UAVImg

from utils import flatten

sys.path.append(os.path.expanduser("~/data/uav-commands/image_assoc"))
from assoc import load_assoc, match

random.seed(1337)
BASE_PATH = os.path.expanduser("~/data")


def load_command_image_assoc_test():
    path = os.path.join(BASE_PATH, "uav-commands", "uav-command-image-assoc.csv")
    with open(path, "r") as f:
        reader = csv.reader(f, delimiter=",")
        _ = next(reader)  # Drop header
        return [row for row in reader]


def load_test_commands():
    folder = os.path.join(BASE_PATH, "uav-commands", "commands")

    def split(line):
        id_, *words = line.split(" ")
        _, _, id_ = id_.split("_")
        command = " ".join(words).strip()
        return id_, command

    def readlines(file_):
        path = os.path.join(folder, file_)
        with open(path, "r") as f:
            return f.readlines()

    lines = flatten(readlines(f) for f in os.listdir(folder) if f.endswith("txt"))
    return [split(line) for line in lines]


def pick_image_imagenet(name, url):
    def remove_missing(files):
        # ImageNet images are downloaded from given URLs, some of them from
        # FlickR. If the image is missing from FlickR, the downloaded image is
        # a "This photo is no longer available" photo. This template photo has
        # a size of 7115 bytes and I'm using this information to exclude it
        # from the picking process.
        return [f for f in files if os.path.getsize(os.path.join(path, f)) != 7115]

    # Drop the "imagenet" prefix from the name
    _, *name = name.split("_")
    name = "_".join(name)
    wnid = url.split("=")[1]

    path = os.path.join(BASE_PATH, "imagenet", "imgs", wnid)
    files = os.listdir(path)
    # No need to perform this step again, as it slows down things considerably
    # The cleaning was done at once using the `clean_images.py` script
    # files = remove_missing(files)
    file1 = random.choice(files)

    return os.path.join(path, file1)


def pick_image_mitplaces(name, url, name_to_path=None):
    # Drop the "mitplaces" prefix from the name
    _, *name = name.split("_")
    name = "_".join(name)

    path = random.choice(name_to_path[name])
    files = os.listdir(path)
    file1 = random.choice(files)

    return os.path.join(path, file1)


def load_mitplaces_metadata():
    def prepare_line(line):
        path, *_ = line.strip().split()
        path = path[1:]  # Drop the first '/'
        _, *rest = path.split("/")
        name = "_".join(rest)
        return name, path

    def load_categories(version):
        base_path = os.path.join(BASE_PATH, "mit-places-{}".format(version))
        with open(os.path.join(base_path, "categories.txt"), "r") as f:
            return dict(prepare_line(line) for line in f.readlines())

    name_to_path_both = {v: load_categories(v) for v in (1, 2)}
    keys = set(name_to_path_both[1].keys()) | set(name_to_path_both[2].keys())

    name_to_path = {}
    for k in keys:
        name_to_path[k] = []
        for v in (1, 2):
            try:
                base_path = os.path.join(BASE_PATH, "mit-places-{}".format(v))
                path = os.path.join(base_path, "imgs", name_to_path_both[v][k])
                name_to_path[k].append(path)
            except KeyError:
                pass

    return name_to_path


IMAGE_PICKERS = {
    "imagenet": pick_image_imagenet,
    "mitplaces": partial(pick_image_mitplaces, name_to_path=load_mitplaces_metadata()),
}


def pick_image(assoc, cls):
    type_, *_ = cls.split("_")
    elem = next(e for e in assoc["image-classes"] if e["name"] == cls)
    return IMAGE_PICKERS[type_](**elem)


def create_assocs(assoc, load_commands, verbose=0):

    tags_all = [tag for tag in assoc["tags"]]

    def select_scenarios(tags):
        counter = Counter([tag["scenario"] for tag in tags])
        _, max_freq = counter.most_common(1)[0]
        return [scenario for scenario, freq in counter.items() if freq == max_freq]

    def create_assoc(command):
        tags = [tag for tag in assoc["tags"] if match({"command": command}, tag)]

        if tags:
            # Scenario-specific commands, such as, "detect" or "look for",
            # match all scenarios. In order to avoid selecting pictures
            # that are not relevant for a given command, sample only from
            # those scenarios from which there is the largest agreement
            # among all detections. We achieve this by selecting the most
            # common scenarios.
            scenarios = select_scenarios(tags)
            tags = [tag for tag in tags if tag["scenario"] in scenarios]
            is_matched = True
        else:
            tags = tags_all
            is_matched = False

        tag1 = random.choice(tags)  # Pick tag
        cls1 = random.choice(tag1["image-classes"])  # Pick image class
        img1 = pick_image(assoc, cls1)  # Pick image

        if verbose:
            print("Command:", command)
            print("Tags:", "\n".join([tag["name"] for tag in tags]))
            print("Picked tag:", tag1["name"], "→", cls1)
            print()

        return {
            "command": command,
            "tag": tag1["name"],
            "image-class": cls1,
            "image-path": img1,
            "is-matched": is_matched,
        }

    json_data = [create_assoc(command) for command in load_commands()]
    return json_data


def create_assocs_train(assoc, verbose):
    for n, i in product(SIZES, FOLDS):
        load_commands = lambda: UAVImg("train", n, i).load_sentences()
        json_data = create_assocs(assoc, load_commands, verbose)
        path = os.path.expanduser("~/data/uav-commands/domain_adaptation/assoc")
        path = os.path.join(path, "{}_{}.json".format(n, i))
        with open(path, "w") as f:
            json.dump(json_data, f, indent=4, sort_keys=True)


def create_assocs_test(assoc, verbose):
    test_data = load_test_commands()
    load_commands = lambda: (t[1] for t in test_data)
    json_data = create_assocs(assoc, load_commands, verbose)
    # Augument associations with extra information
    for test_datum, json_datum in zip(test_data, json_data):
        id_, command = test_datum
        json_datum["id"] = id_
        assert command == json_datum["command"]
    # Save generated associations
    path = os.path.expanduser("~/data/uav-commands/domain_adaptation/assoc")
    path = os.path.join(path, "test.json")
    with open(path, "w") as f:
        json.dump(json_data, f, indent=4, sort_keys=True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--split", choices=("train", "test"))
    parser.add_argument("-v", "--verbose", action="count")
    args = parser.parse_args()
    print(args)

    assoc = load_assoc()

    if args.split == "train":
        create_assocs_train(assoc, args.verbose)
    elif args.split == "test":
        create_assocs_test(assoc, args.verbose)


if __name__ == "__main__":
    main()
