Xenos – multi-modal learning from images and text.

# Setting up

First time installation:

```python
virtualenv -p python3.7 venv3
source venv/bin/activate
pip install -r requirements.txt
```

Before each use:

```python
source venv/bin/activate
```

# Examples

Here are some examples of how to use the code:

```bash
python main.py --todo train evaluate --model bi
python -u main.py -d ptb -m uni -t train-opt | tee /tmp/xenos1.log
```

# Related work

This code is based on this [tutorial](https://github.com/yunjey/pytorch-tutorial/blob/master/tutorials/03-advanced/image_captioning).

# Potential improvements

- [x] Simplify the train function by using callbacks.
- [x] Used tied embedding matrices for the RNN decoder.
- [x] Use a form of regularization (dropout or weight decay).
- [x] Should I use `eval` mode?
- [ ] Improve vocabulary size.
- [ ] Make sampling more diverse.
