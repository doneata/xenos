import argparse
import os
import pdb
import pickle

from collections import Counter

import data_loader

from utils import flatten


class Vocabulary(object):
    """Simple vocabulary wrapper."""

    def __init__(self):
        self.word2idx = {}
        self.idx2word = {}
        self.idx = 0
        # Special tokens
        self.TOKENS = {
            "START": "<start>",
            "END": "<end>",
            "UNK": "<unk>",
            "PAD": "<pad>",
        }

    def add_word(self, word):
        if not word in self.word2idx:
            self.word2idx[word] = self.idx
            self.idx2word[self.idx] = word
            self.idx += 1

    def __call__(self, word):
        if not word in self.word2idx:
            return self.word2idx[self.TOKENS["UNK"]]
        return self.word2idx[word]

    def __len__(self):
        return len(self.word2idx)


def build_vocab(sentences, select=None):
    """Build a simple vocabulary wrapper."""

    # If the word frequency is less than 'threshold', then the word is discarded.
    words = flatten(s.split() for s in sentences)
    counter = Counter(words)

    if select:
        words = select(counter)
    else:
        words = counter.keys()

    # Create a vocab wrapper and add some special tokens.
    vocab = Vocabulary()

    # Add special tokens.
    for t in vocab.TOKENS.values():
        vocab.add_word(t)

    # Add the words to the vocabulary.
    for word in words:
        vocab.add_word(word)

    return vocab


def get_path(dataset_name, suffix=None):
    key = [dataset_name]
    if suffix:
        key.append(suffix)
    key = '_'.join(key)
    return os.path.join("data", "vocab_{:s}.pkl".format(key))


def get_vocabulary(path):
    with open(path, "rb") as f:
        return pickle.load(f)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--dataset",
        type=str,
        choices=data_loader.DATASETS,
        required=True,
        help="choose a dataset",
    )
    parser.add_argument(
        "-s", "--select", type=str, default="", help="how to select top words: thresh-{:d} or topk-{:d}"
    )
    args = parser.parse_args()

    Dataset = data_loader.DATASETS[args.dataset]
    dataset = Dataset(split='train')
    sentences = dataset.load_sentences()

    if args.select.startswith('thresh'):
        _, thresh = args.select.split('-')
        thresh = int(thresh)
        select = lambda counter: [word for word, count in counter.items() if count >= thresh]
        suffix = 'thresh_{:d}'.format(thresh)
    elif args.select.startswith('topk'):
        _, topk = args.select.split('-')
        topk = int(topk)
        select = lambda counter: [word for word, _ in counter.most_common(topk)]
        suffix = 'topk_{:d}'.format(topk)
    else:
        select = None
        suffix = None

    vocab = build_vocab(sentences, select)
    vocab_path = get_path(args.dataset, suffix)

    with open(vocab_path, "wb") as f:
        pickle.dump(vocab, f)

    print("Total vocabulary size: {}".format(len(vocab)))
    print("Saved the vocabulary wrapper to '{}'".format(vocab_path))


if __name__ == "__main__":
    main()
