import os, errno

from typing import Iterable, List, TypeVar

A = TypeVar("A")


def flatten(xss: Iterable[Iterable[A]]) -> List[A]:
    return [x for xs in xss for x in xs]


def subsample(xs: Iterable[A], n: int) -> Iterable[A]:
    return (x for i, x in enumerate(xs) if i % n == 0)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def symlink_force(target, link_name):
    try:
        os.symlink(target, link_name)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(link_name)
            os.symlink(target, link_name)
        else:
            raise e
