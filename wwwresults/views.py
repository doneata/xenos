import csv
import itertools
import json
import os
import pdb
import sys

from itertools import groupby, zip_longest

from flask import render_template

import numpy as np

from . import app

sys.path.append('.')
from score_sentence import load_image_command_test_assocs



ASSOC_TYPE = os.environ.get("ASSOC_TYPE", "annotated")
SIZE = int(os.environ.get("SIZE", "2048"))
FOLD = int(os.environ.get("FOLD", "1"))

BASE = "/home/doneata/data"
EXP_BASE_PATH = "/home/doneata/work/experiments-tedlium-r2"
DECODING_PATH = os.path.join(
    EXP_BASE_PATH,
    "exp_uav_domain_adaptation",
    "n-gram/{}_{}/chain_cleaned/tdnn1f_sp_bi".format(SIZE, FOLD),
    "decode_test_uav_rescore_rnn.xenos-{}{}",
)
ARCHIVES_PATH = os.path.join(DECODING_PATH, "archives.{}")


def grouper(n, iterable, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)


def flatten(xss):
    return list(itertools.chain.from_iterable(xss))


def load_assocs_annotated():
    def to_dict(row):
        _, annotator, id_, command, *_ = row
        _, id_ = id_.split("_")
        path = "uav-commands/images/{:04d}.jpg".format(int(id_))
        if os.path.exists(os.path.join(BASE, path)):
            path = "/static/data/" + path
        else:
            path = 'http://placehold.it/256x256'
        return {
            "id": id_,
            "command": command,
            "annotator": annotator,
            "image-path": path,
        }
    path = os.path.join(BASE, "uav-commands", "uav-command-image-assoc.csv")
    with open(path, 'r') as f:
        reader = csv.reader(f, delimiter=",")
        _ = next(reader)
        data = [to_dict(row) for row in reader]
    return data


def load_assocs_generated():
    def to_dict(key, val):
        path = val["image-path"]
        path = "/".join(path.split("/")[4:])
        if os.path.exists(os.path.join(BASE, path)):
            path = "/static/data/" + path
        else:
            path = 'http://placehold.it/256x256'
        return {
            "id": key,
            "command": val["command"],
            "image-path": path,
        }
    test_assocs = load_image_command_test_assocs()
    data = [to_dict(*item) for item in test_assocs.items()]
    return data


def load_images():
    dir_ = os.path.join(BASE, "uav-commands", "images")
    images = os.listdir(dir_)
    images = [im.split('.')[0] for im in images if im.endswith('.jpg')]
    return set(images)


def split_id(id_):
    id1, id2 = id_.split('-')
    _, _, id1 = id1.split('_')
    return id1, id2


def group_by_fst(xs):
    f = lambda t: t[0]
    xs = sorted(xs, key=f)
    return dict([(k, list(g)) for k, g in groupby(xs, key=f)])


def load_n_best(i):
    def prepare(line):
        id_, *words = line.split(" ")
        id1, id2 = split_id(id_)
        sentence = " ".join(words).strip()
        return id1, id2, sentence
    path = os.path.join(ARCHIVES_PATH.format("text", "", i), "words_text")
    with open(path, 'r') as f:
        return [prepare(line) for line in f.readlines()]


def load_rnn_scores(type_, i):
    def prepare(line):
        id_, score = line.split(" ")
        id1, id2 = split_id(id_)
        score = - float(score)
        return id1, id2, score
    suffix = "" if type_ == "text" else ("-" + ASSOC_TYPE)
    path = os.path.join(ARCHIVES_PATH.format(type_, suffix, i), "lmwt.rnn")
    with open(path, 'r') as f:
        return [prepare(line) for line in f.readlines()]


def load_index_to_word_map():
    def parse(row):
        word, index = row.strip().split()
        return index, word
    path = os.path.join(EXP_BASE_PATH, "data/lang/words.txt")
    with open(path, 'r') as f:
        return dict(parse(row) for row in f.readlines())


def load_transcriptions(index_to_word, type_):
    def parse(row):
        id1, *indices = row.strip().split()
        _, _, id1 = id1.split("_")
        sentence = " ".join(index_to_word[i] for i in indices)
        return id1, sentence
    suffix = "" if type_ == "text" else ("-" + ASSOC_TYPE)
    path = os.path.join(DECODING_PATH.format(type_, suffix), "scoring", "10.tra")
    with open(path, 'r') as f:
        return dict(parse(row) for row in f. readlines())


def load_data():
    if ASSOC_TYPE == "annotated":
        load_assocs = load_assocs_annotated
    elif ASSOC_TYPE == "generated":
        load_assocs = load_assocs_generated
    else:
        assert False

    assocs = load_assocs()
    images = load_images()

    n_best = group_by_fst(flatten(load_n_best(i) for i in range(1, 9)))

    rnn_scores_txt = group_by_fst(flatten(load_rnn_scores("text", i) for i in range(1, 9)))
    rnn_scores_img = group_by_fst(flatten(load_rnn_scores("img",  i) for i in range(1, 9)))

    index_to_word = load_index_to_word_map()

    transcriptions_txt = load_transcriptions(index_to_word, "text")
    transcriptions_img = load_transcriptions(index_to_word, "img")

    def get_rank(t):
        t = - np.array(t)
        return t.argsort().argsort() + 1

    def get_col(data, c):
        return [row[c] for row in data]

    def zip_n_best(n_best, scores_txt, scores_img, pred_txt, pred_img):
        # Sanity checks – check that the all three input sources are in the
        # same order
        for c in (0, 1):
            xyz = zip(
                get_col(n_best, c),
                get_col(scores_txt, c),
                get_col(scores_img, c),
            )
            assert all(x == y == z for x, y, z in xyz)
        # Extract only useful information
        commands = get_col(n_best, 2)
        scores_txt = get_col(scores_txt, 2)
        scores_img = get_col(scores_img, 2)
        # Add ranks
        ranks_txt = get_rank(scores_txt)
        ranks_img = get_rank(scores_img)
        # Add info if the command was predicted
        is_pred_txt = [command == pred_txt for command in commands]
        is_pred_img = [command == pred_img for command in commands]
        # Glue together all info
        data = zip(commands, ranks_txt, scores_txt, is_pred_txt, ranks_img, scores_img, is_pred_img)
        data = enumerate(data, start=1)
        return list(data)

    def is_correct(n_best_data, pred):
        pdb.set_trace()

    def select(assoc):
        id_ = assoc["id"]
        pred_txt = transcriptions_txt[id_]
        pred_img = transcriptions_img[id_]
        n_best_data = zip_n_best(n_best[id_], rnn_scores_txt[id_], rnn_scores_img[id_], pred_txt, pred_img)
        return {
            "id": id_,
            "image-path": assoc["image-path"],
            "command": assoc["command"],
            "n-best": n_best_data,
            "predicted": {
                "txt": pred_txt,
                "img": pred_img,
            },
            "is-correct": {
                "txt": assoc["command"] == pred_txt,
                "img": assoc["command"] == pred_img,
            }
        }

    assocs = sorted(assocs, key=lambda d: d["id"])

    if ASSOC_TYPE == "annotated":
        assocs = [assoc for assoc in assocs if assoc["id"] in images]

    data = [select(assoc) for assoc in assocs]
    data = [datum for datum in data if datum["is-correct"]["img"] != datum["is-correct"]["txt"]]
    return data


@app.route('/')
def index():
    title = "UAV Results"
    data = load_data()
    return render_template("index.html", title=title, data=data)
