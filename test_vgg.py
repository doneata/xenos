import json
import os
import pdb

from PIL import Image

from matplotlib import pyplot as plt

from mpl_toolkits.axes_grid1 import ImageGrid

import numpy as np

import torch

from torch.autograd import Variable

from torch.utils.data import DataLoader

from torchvision import transforms

import torchvision.models as models

from data_loader import (
    PATH,
    FlickR,
)


INPUT_SIZE = 224
MEAN = [0.485, 0.456, 0.406]
STD = [0.229, 0.224, 0.225]


with open(os.path.join(PATH, "imagenet_class_index.json"), "r") as f:
    imagenet_labels = json.load(f)


def imshow(inp):
    inp = inp.cpu().numpy().transpose((1, 2, 0))
    inp = np.array(STD) * inp + np.array(MEAN)
    inp = np.minimum(np.maximum(inp, 0.0), 1.0)
    plt.imshow(inp)


if __name__ == '__main__':

    idx_to_label = [imagenet_labels[str(k)][1] for k in range(len(imagenet_labels))]
    NR_CLASSES = len(imagenet_labels)

    normalize = transforms.Normalize(mean=MEAN, std=STD)
    data_transform = transforms.Compose(
        [
            transforms.Resize(INPUT_SIZE),
            transforms.CenterCrop(INPUT_SIZE),
            transforms.ToTensor(),
            normalize,
        ]
    )
    dataset = FlickR(split="train", transform=data_transform)
    data_loader = DataLoader(dataset, batch_size=8, shuffle=True)

    batch, captions = next(iter(data_loader))
    vgg16 = models.vgg16(pretrained=True)

    if torch.cuda.is_available():
        vgg16 = vgg16.cuda()
        batch = Variable(batch.cuda())
    else:
        batch = Variable(batch)

    preds = vgg16(batch)

    fig = plt.figure(1, figsize=(16, 4))
    top_k = 10

    for img, caption, pred in zip(batch, captions, preds):
        imshow(img)

        print("Top {:d} best-scoring ImageNet classes:".format(top_k))
        ps, ix = pred.sort()
        for i in range(NR_CLASSES - 1, NR_CLASSES - top_k, -1):
            print("→ {:5.2f} {:s}".format(ps[i], idx_to_label[ix[i]]))

        print("Captions:")
        for c in caption.split("\n"):
            print(">", c)

        print()
        plt.show()
