import os
import pdb
import pickle

from collections import Counter

from build_vocab import Vocabulary, build_vocab

from data_loader import Cantab, UAV

select = lambda counter: [word for word, _ in counter.most_common(10000)]
sentences1 = Cantab("train").sentences
vocab1 = build_vocab(sentences1, select) 

sentences2 = UAV("test").sentences
vocab2 = build_vocab(sentences2)

# Merge dictionaries
for word in vocab2.word2idx.keys():
    vocab1.add_word(word)

vocab_path = os.path.join("data", "vocab_cantab_uav.pkl")
with open(vocab_path, "wb") as f:
    pickle.dump(vocab1, f)

print("Total vocabulary size: {}".format(len(vocab1)))
print("Saved the vocabulary wrapper to '{}'".format(vocab_path))
